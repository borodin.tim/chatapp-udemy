const express = require('express');
const http = require('http');
const path = require('path');
const socketio = require('socket.io');
const Filter = require('bad-words');
const { generateMessage, generateLocationMessage } = require('./utils/messages');
const { addUser, removeUser, getUser, getUsersInRoom } = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const publicDirectoryPath = path.join(__dirname, '../public');

app.use(express.static(publicDirectoryPath));

io.on('connection', (socket) => {
    console.log('New web socket connection');

    // socket.on('join', ({ username, room }, aknowledgeCallback) => {
    socket.on('join', (options, aknowledgeCallback) => {
        const { error, user } = addUser({ id: socket.id, ...options });
        if (error) {
            return aknowledgeCallback(error);
        }
        socket.join(user.room);
        socket.emit('message', generateMessage('Administrator', 'Welcome!'));
        socket.broadcast.to(user.room).emit('message', generateMessage('Administrator', `${user.username} has joined!`));
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room)
        });
        aknowledgeCallback();
    });

    socket.on('sendMessage', (message, aknowledgeCallback) => {
        const user = getUser(socket.id);
        if (!user) {
            return aknowledgeCallback(error);
        }
        const filter = new Filter();
        if (filter.isProfane(message)) {
            return aknowledgeCallback('Profanity is not allowed');
        }
        io.to(user.room).emit('message', generateMessage(user.username, message));
        aknowledgeCallback();
    });

    socket.on('sendLocation', (coords, aknowledgeCallback) => {
        const user = getUser(socket.id);
        if (!user) {
            return aknowledgeCallback(error);
        }
        io.to(user.room).emit('locationMessage', generateLocationMessage(user.username, `https://google.com/maps?q=${coords.latitude},${coords.longitude}`));
        aknowledgeCallback('Location shared!');
    })

    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        if (user) {
            io.to(user.room).emit('message', generateMessage('Administrator', `${user.username} has left`));
            io.to(user.room).emit('roomData', { 
                room: user.room,
                users: getUsersInRoom(user.room)
            });
        }
    });
});

module.exports = {
    app,
    server
};
